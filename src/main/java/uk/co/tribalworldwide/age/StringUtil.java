package uk.co.tribalworldwide.age;

public class StringUtil {

	private static final long ONE_SECOND = 1000;
	private static final long ONE_MINUTE = 60 * ONE_SECOND;
	private static final long ONE_HOUR = 60 * ONE_MINUTE;
	private static final long ONE_DAY = ONE_HOUR * 24;
	private static final long ONE_WEEK = ONE_DAY * 7;
	
	/** Returns the age parameter in the following display format
	 * format is    0m
	 *              9m
	 *              14h 59m
	 *              1d 4h 26m
	 *              1w 2d 5h 23m
	 **/
	public static String formatAge(long age) {
	    StringBuilder sb = new StringBuilder();
	    if(age>= ONE_WEEK) {
	        int weeks = (int) Math.floor(age/ONE_WEEK);
	        sb.append(weeks);
	        sb.append("w ");
	        age -= weeks*ONE_WEEK;
	    }
	    if(age >= ONE_DAY) {
	        int days = (int) Math.floor(age/ONE_DAY);
	        sb.append(days);
	        sb.append("d ");
	        age -= days*ONE_DAY;
	    }
	    else {
	        // if we have week data so far, then add a zero
	        if(sb.length()>0)
	            sb.append("0d ");
	    }
	    if(age >= ONE_HOUR){
	        int hours = (int) Math.floor(age/ONE_HOUR);
	        sb.append(hours);
	        sb.append("h ");
	        age -= hours*ONE_HOUR;
	    }
	    else {
	        // if we have day data so far, then add a zero
	        if(sb.length()>0)
	            sb.append("0h ");
	    }
	    if(age >= ONE_MINUTE){
	        int mins = (int) Math.floor(age/ONE_MINUTE);
	        sb.append(mins);
	        sb.append("m");
	    }
	    else {
	        sb.append("0m");
	    }
	    return sb.toString();
	}
	
}

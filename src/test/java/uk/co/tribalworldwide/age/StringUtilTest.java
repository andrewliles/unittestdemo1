package uk.co.tribalworldwide.age;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static uk.co.tribalworldwide.age.StringUtil.formatAge;

import org.junit.Test;

public class StringUtilTest {

	@Test
	public void formatAge_Negative() {
		assertThat(formatAge(-1L), is("0m"));
	}

	@Test
	public void formatAge_Zero() {
		assertThat(formatAge(0L), is("0m"));
	}

	@Test
	public void formatAge_LessThanSecond() {
		assertThat(formatAge(999L), is("0m"));
	}

	@Test
	public void formatAge_LessThanMinute() {
		assertThat(formatAge(55 * 1000), is("0m"));
	}

	@Test
	public void formatAge_1To2Minutes() {
		assertThat(formatAge(82 * 1000), is("1m"));
	}

	@Test
	public void formatAge_LessThanAnHour() {
		assertThat(formatAge(16 * 60 * 1000), is("16m"));
		assertThat(formatAge(59 * 60 * 1000), is("59m"));
		assertThat(formatAge(1 * 60 * 60 * 1000 - 1), is("59m"));
	}

	@Test
	public void formatAge_SeveralHours() {
		assertThat(formatAge(1 * 60 * 60 * 1000), is("1h 0m"));
		assertThat(formatAge(1 * 60 * 60 * 1000 + 1), is("1h 0m"));
		assertThat(formatAge(1 * 60 * 60 * 1000 + 1 * 60 * 1000), is("1h 1m"));
		assertThat(formatAge(1 * 60 * 60 * 1000 + 28 * 60 * 1000), is("1h 28m"));

		assertThat(formatAge(2 * 60 * 60 * 1000 + 0 * 60 * 1000), is("2h 0m"));
		assertThat(formatAge(9 * 60 * 60 * 1000 + 59 * 60 * 1000), is("9h 59m"));
		assertThat(formatAge(23 * 60 * 60 * 1000 + 59 * 60 * 1000), is("23h 59m"));
		assertThat(formatAge(1 * 24 * 60 * 60 * 1000 - 1), is("23h 59m"));
	}

	@Test
	public void formatAge_MoreThanADay() {
		assertThat(formatAge(1 * 24 * 60 * 60 * 1000 + 7 * 60 * 60 * 1000 + 6 * 60 * 1000), is("1d 7h 6m"));
		assertThat(formatAge(((long) 44 * 24 * 60 * 60 * 1000) + ((long) 2 * 60 * 60 * 1000 + 50 * 60 * 1000)),
				is("6w 2d 2h 50m"));
		assertThat(formatAge(((long) 700 * 24 * 60 * 60 * 1000) + ((long) 2 * 60 * 60 * 1000 + 50 * 60 * 1000)),
				is("100w 0d 2h 50m"));
	}
}
